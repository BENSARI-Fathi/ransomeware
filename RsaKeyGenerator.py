from Crypto.PublicKey import RSA
from termcolor import colored


class RsaKeyGenerator:
    def __init__(self, pubKeyFilename="public.pem", privateKeyFilename="private.pem", debug=True):
        self.publicKeyFilename = pubKeyFilename
        self.privateKeyFilename = privateKeyFilename
        self.debug = debug

    def generate(self):
        key = RSA.generate(2048)
        privateKey = key.export_key()
        publicKey = key.public_key().export_key()

        with open(self.publicKeyFilename, "wb") as f:
            f.write(publicKey)
            if self.debug:
                print(colored(f"[INFO] Public key saved to {self.publicKeyFilename}", "green"))

        with open(self.privateKeyFilename, "wb") as f:
            f.write(privateKey)
            if self.debug:
                print(colored(f"[INFO] Private key saved to {self.privateKeyFilename}", "green"))


if __name__ == "__main__":
    generator = RsaKeyGenerator()
    generator.generate()
